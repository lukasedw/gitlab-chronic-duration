module ChronicDuration
  # '0.10.6' is the versioning used in 'https://github.com/henrypoydar/chronic_duration',
  # we are adding revision part to highlight GitLab changes, e.g. '0.10.6.N'
  VERSION = '0.10.6.2'.freeze
end
